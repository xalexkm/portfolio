import React from "react";
import { render } from "react-dom";
import App from "./components/App";
import "./styles.scss";

import "bootstrap";

render(
  <>
    <App />
  </>,
  document.getElementById("root")
);
