import React, { Component } from "react";
import Head from "./Head";
import About from "./About";
import { Works } from "./Works";
import Contact from "./Contact";
import Gallery from "./Gallery";
import "./App.scss";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: false,
      selectedItem: undefined,
    };
    this.getSelectedItem = this.getSelectedItem.bind(this);
    this.handleSelectedItem = this.handleSelectedItem.bind(this);
  }
  componentDidMount() {
    setTimeout(() => {
      this.setState({
        ready: true,
      });
    }, 1000);
  }

  getSelectedItem(selectedItem) {
    this.setState({
      selectedItem: selectedItem,
    });
  }
  handleSelectedItem() {
    this.setState({ selectedItem: undefined });
  }

  render() {
    return (
      <div className="app--wrapper">
        <Head />
        <hr className="divider" />
        <About />
        <div className="jagged-divider d-flex">
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
          <div className="jag"></div>
        </div>
        <Works
          liftSelectedItem={this.getSelectedItem}
          selectedItem={this.state.selectedItem}
        />
        <Contact />
        <Gallery />
        {this.state.selectedItem !== undefined ? (
          <div
            style={{
              opacity: this.state.selectedItem !== undefined ? 0.7 : 0,
            }}
            className="works-tab--backdrop"
            onClick={this.handleSelectedItem}
          ></div>
        ) : (
          ""
        )}

        {/* Post effects */}

        <span className="film-grain"></span>
        <span
          style={{ opacity: this.state.ready ? 1 : 0 }}
          className="vignette"
        ></span>
      </div>
    );
  }
}
