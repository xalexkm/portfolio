import React from "react";
import "./Contact.scss";
import cv from "../assets/CV22.pdf";
const Contact = () => {
  return (
    <section className="contact--wrapper">
      <div className="container-lg">
        <div className="contact--hor-line"></div>
        <div className="contact--quote">{`// YOU MADE IT`}</div>
        <div className="contact--item-block d-flex flex-wrap justify-content-center align-items-center">
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://bitbucket.org/xalexkm/"
          >
            <div className="contact--item">
              <svg width="160" height="80" className="ptf--fancy-button">
                <rect
                  width="160"
                  height="80"
                  fill="none"
                  className="ptf--fancy-button__rect"
                ></rect>
              </svg>
              <span>BITBUCKET</span>
            </div>
          </a>

          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://www.instagram.com/lonelessxd/"
          >
            <div className="contact--item">
              <svg width="160" height="80" className="ptf--fancy-button">
                <rect
                  width="160"
                  height="80"
                  fill="none"
                  className="ptf--fancy-button__rect"
                ></rect>
              </svg>
              <span>INSTAGRAM</span>
            </div>
          </a>

          <a target="_blank" rel="noopener noreferrer" href={cv}>
            <div className="contact--item">
              <svg width="160" height="80" className="ptf--fancy-button">
                <rect
                  width="160"
                  height="80"
                  fill="none"
                  className="ptf--fancy-button__rect"
                ></rect>
              </svg>
              <span>CV</span>
            </div>
          </a>
        </div>
      </div>
    </section>
  );
};

export default Contact;
