import React from "react";
import "./Gallery.scss";

import gallery_iron from "../assets/gallery_iron.jpg";
import getterclan_logo from "../assets/getterclan_logo.jpg";
import keep_away_sign from "../assets/keep_away_sign.jpg";
import toilet_sign from "../assets/toilet_sign.jpg";
import party_sign from "../assets/party_sign.jpg";
import mgk_finish from "../assets/mgk_finish.jpg";

const Gallery = () => {
  const gallery_media = [
    {
      name: "Toilet sign",
      source: toilet_sign,
    },
    {
      name: "Mgk finish",
      source: mgk_finish,
    },
    {
      name: "Party sign",
      source: party_sign,
    },
    {
      name: "Getterclan logo",
      source: getterclan_logo,
    },
    {
      name: "Iron logo",
      source: gallery_iron,
    },
    {
      name: "Keep away sign",
      source: keep_away_sign,
    },
  ];
  return (
    <div className="gallery--wrapper d-flex">
      <div className="gallery--content d-flex flex-nowrap justify-content-center">
        {gallery_media.map((data, index) => (
          <div key={index} className="gallery--piece">
            <img src={data.source} alt={data.name} />
          </div>
        ))}
      </div>
      <div className="gallery--overlay"></div>
      <div className="powered-by d-flex flex-column flex-nowrap align-items-center">
        <span>Proudly Powered by React</span>
        <span>Contact me</span>
        <span>xalexkm@gmail.com</span>
      </div>
    </div>
  );
};

export default Gallery;
