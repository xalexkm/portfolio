import "./WorksTab.scss";
import React, { useEffect, useRef } from "react";
import { PropTypes } from "prop-types";

import bitbucket_logo from "../assets/bitbucket_logo.png";

const WorksTab = ({ selectedMedia, selectItem, selectedItem }) => {
  const videoRef = useRef();
  useEffect(() => {
    videoRef.current?.load();
  }, [selectedMedia]);
  return (
    <>
      <div
        className="works-tab"
        style={{
          opacity: selectedItem !== undefined ? 1 : 0,
        }}
      >
        <div className="works-tab--wrapper">
          <div className="works--media">
            {selectedMedia.media.includes("/image") ? (
              <img width="100%" src={selectedMedia.media} />
            ) : (
              <video width="100%" loop autoPlay muted controls ref={videoRef}>
                <source src={selectedMedia.media} type="video/mp4" />
              </video>
            )}
          </div>
          <div className="works--details">{selectedMedia.details}</div>
          <div className="works--exit" onClick={() => selectItem(undefined)}>
            <svg>
              <line x1="0" y1="15" x2="15" y2="0" strokeWidth="3" />
              <line x1="0" y1="0" x2="15" y2="15" strokeWidth="3" />
            </svg>
          </div>
          <div className="works--source">
            <a
              target="_blank"
              rel="noopener noreferrer"
              href={selectedMedia.source_link}
            >
              <img src={bitbucket_logo}></img>
            </a>
          </div>
          <div className="works--tech-stack">
            {selectedMedia.tech_stack.map((data, index) => (
              <img key={index} src={data} />
            ))}
          </div>
        </div>
      </div>
    </>
  );
};
WorksTab.propTypes = {
  selectedMedia: PropTypes.object,
  selectItem: PropTypes.func,
  selectedItem: PropTypes.any,
};

export default WorksTab;
