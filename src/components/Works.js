// Asset import
import plann_preview from "../assets/plann_preview.mp4";
import alize_preview from "../assets/alize_preview.mp4";
import fitness_app_preview from "../assets/fitness_app_preview.mp4";
import ng_forms_preview from "../assets/ng_forms_preview.mp4";
import carousel_preview from "../assets/carousel_preview.mp4";
import infscroll_preview from "../assets/infscroll_preview.mp4";
import fitrack_preview from "../assets/fitrack_preview.mp4";
import weather_app_preview from "../assets/weather_app_preview.mp4";
import card_verify_preview from "../assets/card_verify_preview.mp4";

import angular from "../assets/angular_logo.png";
import react from "../assets/react_logo.png";
import express from "../assets/express_logo.png";
import node from "../assets/node_logo.png";
import mongo from "../assets/mongo_logo.png";
import js from "../assets/js_logo.png";
import ts from "../assets/ts_logo.png";
import ngrx from "../assets/ngrx_logo.png";
import redux from "../assets/redux_logo.png";
import html from "../assets/html_logo.png";
import sass from "../assets/sass_logo.png";
import ionic from "../assets/ionic_logo.png";
import jasmine from "../assets/jasmine_logo.svg";
import jest from "../assets/jest_logo.png";

// Styles import
import "./Works.scss";
// Lib imports
import React, { Component } from "react";
import WorksTab from "./WorksTab";
import Parallax from "parallax-js";
import { PropTypes } from "prop-types";

export class Works extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: this.props.selectedItem,
    };
    this.selectItem = this.selectItem.bind(this);
  }

  componentDidMount() {
    var scene = document.getElementById("works_parallax");
    var parallaxInstance = new Parallax(scene);
  }

  // dropSelectedItem() {
  // }
  data = [
    {
      name: "Plann",
      media: plann_preview,
      tech_stack: [mongo, express, angular, node, ts, html, sass, ngrx],
      source_link: "https://bitbucket.org/xalexkm/plann/src/master/",
      details:
        "This project uses MEAN stack. I have also used NGRX for state management. You are able to add, delete and modify (set a pending or completed state) items in the list. Items are synced and are saved in the database automatically.",
    },
    {
      name: "Alize",
      media: alize_preview,
      tech_stack: [react, redux, node, js, html, sass],
      source_link: "https://bitbucket.org/xalexkm/alize/src/master/",
      details:
        "This project was created using React, with Redux for state management. Items are synced with a database using a local JSONServer service. You can add, delete and modify items in the list. You can add colour label to an item and filter the whole list based on the colour code.",
    },
    {
      name: "Fitness App",
      media: fitness_app_preview,
      tech_stack: [angular, ionic, ts, html, sass, node],
      source_link:
        "https://bitbucket.org/DmitryKmita/fitness-app-483ld2/src/master/",
      details:
        "Here I have built front end for an authentication and an introductory questionnaire for a mobile app. The project uses Ionic SDK and Angular. I have implemented verification to the authentication page, added multiple custom inputs for body dimensions (height, weight).",
    },
    {
      name: "Carousel",
      media: carousel_preview,
      tech_stack: [js, sass, html],
      source_link: "https://bitbucket.org/xalexkm/purejs/src/master/",
      details:
        "This project is a carousel created with pure JavaScript. It uses a public image API in order to populate the caroussel on load. I have used Bootstrap for styling.",
    },
    {
      name: "Infscroll",
      media: infscroll_preview,
      tech_stack: [js, sass, html],
      source_link: "https://bitbucket.org/xalexkm/purejs/src/master/",
      details:
        "This project is a simple infinite scroll created with pure JavaScript. It uses public dog breed API in order to fetch data for display on demand. It uses Bootstrap for styling.",
    },
    {
      name: "Fitrack",
      media: fitrack_preview,
      tech_stack: [mongo, express, angular, node, ts, html, sass],
      source_link: "https://bitbucket.org/xalexkm/fitrack/src/master/",
      details:
        "This project is a dashboard for a fitness and nutrition tracking app created with MEAN stack, and uses chart.js library for infographics. Dashboard is fully responsive for the common screen sizes and devices. Here I have created a custom SVG widget for displaying nutrition proportions.",
    },
    {
      name: "NG Forms",
      media: ng_forms_preview,
      tech_stack: [angular, ts, html, sass],
      source_link:
        "https://bitbucket.org/xalexkm/ng-reactive-forms/src/master/",
      details:
        "This form is created with Angular Reactive Forms and styled with Bootstrap.",
    },
    {
      name: "Weather App",
      media: weather_app_preview,
      tech_stack: [ts, sass, html, angular, jasmine],
      source_link: "https://bitbucket.org/xalexkm/weather-app/src/master/",
      details:
        "This project's goal is to display current weather based on user input. The app implements quotes and weather APIs. Quotes are randomly fetched and displayed with custom animation. Quote is split into words and letters and displayed in a pattern. Weather is fetched based on input and metrics are displayed below next to the Google Maps map of the place. App is tested with Jasmine.",
    },
    {
      name: "Card Verify",
      media: card_verify_preview,
      tech_stack: [js, react, html, sass, jest],
      source_link: "https://bitbucket.org/xalexkm/verify-card/src/master/",
      details:
        "Simple card verification created with React and a JavaScript card verification library. Inputs are validated on the go and success is accompanied by a pleasant animation. When entering details into fields, focus is automatically shifted to the next field. Pasting from clipboard will split the card number into the fields automatically. App is tested with Jest.",
    },
  ];

  selectItem(index) {
    this.setState({
      selectedItem: index,
    });
    this.props.liftSelectedItem(index);
  }

  render() {
    return (
      <>
        <section className="works--wrapper">
          <div
            id="works_parallax"
            data-selector=".layer"
            className="works d-flex justify-content-center align-content-center"
          >
            <div
              className="works--content d-flex flex-column layer justify-content-center"
              data-depth="0.2"
            >
              <div
                className="d-flex justify-content-center flex-wrap align-items-center"
                style={{ gap: "20px" }}
              >
                {this.data.map((key, index) => (
                  <div
                    key={index}
                    className="works--item"
                    onClick={() => this.selectItem(index)}
                  >
                    {this.data[index].media.includes("/image") ? (
                      <img
                        height="200"
                        width="300"
                        src={this.data[index].media}
                        alt="Work preview thumbnail"
                      />
                    ) : (
                      <video height="200" width="300" loop autoPlay muted>
                        <source src={this.data[index].media} type="video/mp4" />
                      </video>
                    )}
                    <div className="works--item__overflow"></div>
                    <span>{this.data[index].name}</span>
                  </div>
                ))}
              </div>
              <div
                className="works--content__backlay layer"
                data-depth="0.4"
              ></div>
            </div>
            <div data-depth="0.5" className="works--title layer">
              <span className="">{`React.render(
            <WORKS />);`}</span>
            </div>
            <div data-depth="0.6" className="works--dash layer">
              <div></div>
            </div>
            <div data-depth="0.5" className="works--wow layer">
              <span>WOW</span>
            </div>
          </div>
        </section>
        {this.props.selectedItem !== undefined ? (
          <WorksTab
            selectedItem={this.state.selectedItem}
            selectedMedia={this.data[this.state.selectedItem]}
            selectItem={this.selectItem}
          />
        ) : (
          ""
        )}
      </>
    );
  }
}

Works.propTypes = {
  liftSelectedItem: PropTypes.func,
  selectedItem: PropTypes.number,
};

export default Works;
