import "./Head.scss";
import Parallax from "parallax-js";

import React, { Component } from "react";

export default class Head extends Component {
  componentDidMount() {
    var scene = document.getElementById("head_parallax");
    var parallaxInstance = new Parallax(scene);
  }

  render() {
    return (
      <section className="head--wrapper">
        <div id="head_parallax" data-selector=".layer">
          <div>
            <div className="head--title d-flex flex-column justify-content-center align-items-center">
              <div data-depth="0.2" className="head--design layer">
                <div className="d-flex flex-column justify-content-center align-items-center ">
                  <span>CREATIVE</span>
                  <br />
                  <span>FRONT END DEVELOPER</span>
                  <br />
                  <span className="name">ALEX KMITA</span>
                </div>

                <div className="head--triangle">
                  <div className="head--triangle__obj"></div>
                  <div className="head--scroll-arrow">
                    <div className="head--scroll-arrow__wrapper">
                      <div className="head--scroll-arrow__obj"></div>
                    </div>
                    <div className="head--scroll-arrow__text">
                      <span>SCROLL DOWN</span>
                    </div>
                  </div>
                </div>
              </div>
              <span className="head--floaty">
                <span data-depth="0.4" className="layer">
                  UI
                </span>
              </span>
              <span className="head--floaty">
                <span data-depth="0.5" className="layer">
                  UX
                </span>
              </span>
              <span className="head--floaty">
                <span data-depth="0.6" className="layer">
                  DESIGN
                </span>
              </span>

              <svg className="head--upper-ribbon" height="100" width="2000">
                <line
                  x1="0"
                  y1="5"
                  x2="2000"
                  y2="5"
                  strokeWidth="120"
                  stroke="black"
                  strokeDasharray="800 50"
                />
              </svg>

              <svg className="head--lower-ribbon" height="100" width="2000">
                <line
                  x1="0"
                  y1="5"
                  x2="2000"
                  y2="5"
                  strokeWidth="60"
                  stroke="black"
                  strokeDasharray="600 200"
                />
              </svg>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
