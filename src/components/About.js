import "./About.scss";
import cv from "../assets/CV22.pdf";
import React, { Component } from "react";
import picture from "../assets/full_body.jpg";
import Parallax from "parallax-js";

export default class About extends Component {
  componentDidMount() {
    var scene = document.getElementById("about_parallax");
    var parallaxInstance = new Parallax(scene);
  }
  render() {
    return (
      <section
        className="about--wrapper"
        id="about_parallax"
        data-selector=".layer"
      >
        <div
          data-depth="0.1"
          className="about d-flex justify-content-center align-items-center layer flex-wrap"
        >
          <div className="about--resume d-flex flex-column justify-content-between">
            <span className="about--resume__text">
              I am an ambitious front-end developer and a psychology enthusiast
              with a desire to apply my knowledge about mind and human behaviour
              and technical skills to deliver the best products possible,
              benefit other members of the team and grow as a professional.
              Proficient with clients, able to communicate efficiently, and
              build good rapport with a diverse range of individuals. I commit
              to the teams I work in, take responsibilities that are expected of
              me, and encourage collaboration between the members of the team. I
              am committed to share my knowledge with my colleagues and take in
              any individuals ideas in order to come to the best resolution of
              the problem and learn from it.
              <br />
              <br />
              On the technical side I am proficient in several languages such as
              HTML, CSS, JavaScript (as well as Typescript) and its subsequent
              frameworks: React, Angular and Vue. I have experience with state
              management tools like Redux and NGRX, and testing tools like
              Jasmine and Jest. On the backend, I have used NodeJS, Express and
              MongoDB, as well as, PHP and mySQL.
            </span>
            <div className="d-flex justify-content-around align-items-center">
              <a
                className="about--cv"
                target="_blank"
                rel="noopener noreferrer"
                href={cv}
              >
                <svg width="160" height="80" className="ptf--fancy-button">
                  <rect
                    width="160"
                    height="80"
                    fill="none"
                    className="ptf--fancy-button__rect"
                  ></rect>
                </svg>
                <span>CV</span>
              </a>
              <a
                className="about--cv"
                target="_blank"
                rel="noopener noreferrer"
                href="https://www.linkedin.com/in/alexkmita/"
              >
                <svg width="160" height="80" className="ptf--fancy-button">
                  <rect
                    width="160"
                    height="80"
                    fill="none"
                    className="ptf--fancy-button__rect"
                  ></rect>
                </svg>
                <span>LinkedIn</span>
              </a>
            </div>
          </div>
          <div className="about--photo">
            <img
              height="500"
              width="350"
              src={picture}
              alt="Full body photo of me"
            />
            <div className="about--photo__backlay" data-depth="0.01"></div>
          </div>
          <div className="about--title">
            <span data-depth="0.4" className="layer">
              {"<ABOUT />"}
            </span>
          </div>
        </div>
      </section>
    );
  }
}
